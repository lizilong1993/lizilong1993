# 名人名言


***记录一下我觉得有意思的名人名言和典故。***

<!--more-->

{{< admonition tip "提示" false>}}
 本文档只为记录个人遇到的名言警句和典故，如若侵权请务必在评论区或直接[联系我](lizilong@whu.edu.cn)删除！

{{< /admonition >}}

## 1 名人

这里记录公认或个人认为的名人。

### 1.1 中国名人

#### 鲁迅

&emsp;&emsp;**周树人**（1881年9月25日－1936年10月19日），原名**周樟寿**，[字](https://zh.wikipedia.org/wiki/表字)**豫山**、**豫亭**，后改字**豫才**，以[笔名](https://zh.wikipedia.org/wiki/笔名)**鲁迅**闻名于世，[浙江](https://zh.wikipedia.org/wiki/浙江省_(清))[绍兴](https://zh.wikipedia.org/wiki/紹興府)人，为[中国](https://zh.wikipedia.org/wiki/中国)近代著名[作家](https://zh.wikipedia.org/wiki/作家)，[新文化运动](https://zh.wikipedia.org/wiki/新文化运动)领袖之一，中国现代文学的奠基人和开山巨匠，亦是在西方世界享有盛誉的中国近代文学家、[思想家](https://zh.wikipedia.org/wiki/思想家)。鲁迅的主要成就包括[杂文](https://zh.wikipedia.org/wiki/杂文)、短中篇[小说](https://zh.wikipedia.org/wiki/小说)、文学、思想和社会评论、学术著作、[自然科学](https://zh.wikipedia.org/wiki/自然科学)著作、古代典籍校勘与研究、[散文](https://zh.wikipedia.org/wiki/散文)、现代散文诗、旧体诗、外国文学与学术[翻译](https://zh.wikipedia.org/wiki/翻译)作品和[木刻版画](https://zh.wikipedia.org/wiki/木刻版画)的研究，对于[五四运动](https://zh.wikipedia.org/wiki/五四运动)以后的中国社会思想文化发展产生了一定的影响，蜚声海外，尤其在[韩国](https://zh.wikipedia.org/wiki/韩国)、[日本](https://zh.wikipedia.org/wiki/日本)思想文化界有着很重要的地位和影响，被韩国[文学评论家](https://zh.wikipedia.org/wiki/文学批评)[金良守](https://zh.wikipedia.org/w/index.php?title=金良守&action=edit&redlink=1)誉为“二十世纪东亚文化地图上占最大领土的作家”，与日本著名的“国民大作家”[夏目漱石](https://zh.wikipedia.org/wiki/夏目漱石)齐名。鲁迅更被誉为中国的“民族魂”[^1]。

> 智能路障：《谈鲁迅合集》

{{< bilibili BV14f4y1E79N >}}

### 1.2 外国名人

## 2 名言

*记录一下读书看剧中看到的有意思的话。*

> 在怒气冲冲的正义使者们之间[^2]，
>
> 你胆敢有一丝怀疑态度和理性看待，
>
> 你就会被当成罪犯。
>
> <p align="right"><font color=green size=3 face="仿宋">佚名 &emsp;</font></p>
----
> “人要是捐了呢?”[^3]
>
> “捐了是作秀”
>
> “不捐?”
>
> “不捐不是人”
>
> “捐的多？”
>
> “张扬”
>
> “捐的少？”
>
> “吝啬”
>
> “那怎么才好呢？”
>
> “都给我才好”
>
> <p align="right"><font color=green size=3 face="仿宋">郭德纲,于谦《你要学好》 &emsp;</font></p>

----

> 一想到为人父母居然不用经过考试，就觉得真是太可怕了。
>
> <p align="right"><font color=green size=3 face="仿宋">日本作家伊坂幸太郎&emsp;</font></p>


## 3 典故

## 4 未完待续

未完待续，欢迎补充！:(far fa-laugh-beam):

觉得有帮助，麻烦顺手给我[点个Star](https://gitee.com/lizilong1993/lizilong1993)吧 or [Buy me a coffee](https://lizilong1993.gitee.io/about/)

## 参考资料{#ref}

万分感谢：

[^1]:[鲁迅 - 维基百科，自由的百科全书 (wikipedia.org)](https://zh.wikipedia.org/wiki/鲁迅)
[^2 ]:["你以为自己道德高尚，别人就不能寻欢作乐了吗？”｜讽刺性极强的哲理句子_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV11R4y1t7gw)
[^3]:郭德纲、于谦《你要学好》


